label tutorial:

    scene initial_room

    python:
        player_name = renpy.input("Bem vindo(a)! Qual é o seu nome?")
        player_name = player_name.strip()
        if not player_name:
            player_name = "Dono(a) do Produto"

    "[player_name] você é um funcionário de uma empresa de desenvolvimento de software"
    python:
        company_name = renpy.input("Qual é o nome da empresa que você irá trabalhar?")
        company_name = company_name.strip()
        if not company_name:
            company_name = "Empresa RenPy"

    "Você decidiu que a melhor forma de desenvolver uma aplicação de sucesso é através da utilização do método Scrum."

    python:
        project_name = renpy.input("Qual será o nome do projeto que você vai coordenar?")
        project_name = project_name.strip()
        if not project_name:
            project_name = "Projeto RenPy"
    "Sua jornada para comandar o [project_name] na [company_name] começa agora!"


    "Por sua experiência na área, você recebeu o papel de Dono do Produto!"

    "Você não estará sozinho! Carlos, um funcionário com muita experiência no processo de Scrum, se aproxima"

    show businessman

    c "Olá [player_name]! É uma honra trabalhar com você!"

    c "Como o mestre de Scrum da equipe, gostaria de relembrar as funções de nossos papéis?"

label explanation:
    menu:

        "Me relembre, o quê um Dono de Produto faz?":
            jump PO_definition

        "Me relembre, o quê um Mestre de Scrum faz?":
            jump SM_definition

        "Não se preocupe! Ao trabalho!":
            jump start_game

label PO_definition:

    c "O Dono de Produto, ou Product Owner, é um papel da metodologia Scrum"
    c "Ele é responsável por coordenar uma equipe de desenvolvedores para que juntos produzam o [project_name]"
    c "Ele deve cuidar de toda as iterações do projeto, definir histórias de usuários e priorizar atividades"

    jump explanation

label SM_definition:

    c "O Mestre de Scrum, ou Scrum Master, é um papel da metodologia Scrum"
    c "Ele deve atuar como facilitador para que a equipe se comprometa com os princípios desta metodologia ágil"
    c "Ele deverá ajudar os programadores com quaisquer impedimentos na realização das demandas"

    jump explanation

label start_game:
    scene initial_room

    hide businessman

    "Com a divisão de cargos feita, agora é hora de começar seu projeto!"

    show businessman

    c "Primeiro vamos fazer a definição de histórias de usuário para a sua Reserva de Requisitos"
    c "Você e seus desenvolvedores vão orçar a complexidade das tarefas"
    c "E então você deverá definir quais histórias irão entrar na sua primeira corrida"

    hide businesman

    call screen a_fazer
