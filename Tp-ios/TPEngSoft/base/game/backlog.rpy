init python:
    import random
    import constants as ct
    import user_story_class as us
    import project_statistic_class as ps

    ## User stories
    def add_to_sprint(bl_src, values):
        for i in values:
            bl_src.remove(i)
            bl_sprint.append(i)

        for i in reversed(range(len(values))):
            values.pop(i)

    def remove_from_sprint(selected_us):
        user_story = bl_sprint[selected_us]

        if user_story.completed_points == 0:
            bl_todo.append(user_story)
        else:
            bl_in_progress.append(user_story)

        bl_sprint.remove(user_story)

        selected_us = None

    def compute_story_points(story_list):
        for i in story_list:
            i.compute_points()

    def generate_user_stories():
        us_list = [
            us.UserStory("Desenhar interface"),
            us.UserStory("Implementar interface"),
            us.UserStory("Corrigir bug"),
            us.UserStory("Implementar nova característica"),
            us.UserStory("Melhorar o desempenho de uma funcionalidade"),
            us.UserStory("Otimizar código"),
            us.UserStory("Melhorar desempenho gráfico"),
            us.UserStory("Refatorar interface"),
            us.UserStory("Implementar API externa"),
            us.UserStory("Implementar IA"),
            us.UserStory("Refatorar código"),
            us.UserStory("Desenvolver Testes"),
            us.UserStory("Refatorar Testes")
        ]

        return us_list

    def are_points_computed(bl_list, src):
        if(len(bl_list) == 0):
            return False

        for i in bl_list:
            if(i.points == 0):
                return False

        return True

    def prioritize_user_story():
        """Moves a user story one position up (unless it's the first one)"""
        global bl_sprint_selected

        if bl_sprint_selected > 0:
            bl_sprint[bl_sprint_selected-1], bl_sprint[bl_sprint_selected] = bl_sprint[bl_sprint_selected], bl_sprint[bl_sprint_selected-1]
            bl_sprint_selected -= 1



    bl_todo = generate_user_stories()
    bl_todo_selected = []

    bl_sprint = []
    bl_sprint_selected = None

    bl_in_progress = []
    bl_in_progress_selected = []

    bl_validation = []
    bl_validation_selected = []

    bl_done = []
    bl_done_selected = []

    # Lists for the sprint result screen
    completed_this_sprint = []
    not_completed_this_sprint = []
    overall_completion_percentage = 0

    n_iteration = 1

    ## Project statistics

    def generate_statistics():
        stats_list = [
            ps.ProjectStatistic("Conclusão do projeto", 0, "%"),
            ps.ProjectStatistic("Histórias de usuário por iteração", 0),
            ps.ProjectStatistic("Velocidade do time", 0),
            ps.ProjectStatistic("Problemas por iteração", 0),
            ps.ProjectStatistic("Satisfação do cliente", 0),
            ps.ProjectStatistic("Estabilidade do código", 0),
            ps.ProjectStatistic("Motivação do time", 0)
        ]

        return stats_list

    pj_stats = generate_statistics()

    # Mean and standard deviation of how many user stories are completed
    # Could be changed in later features
    sp_mean = 10
    sp_std = 2

    def compute_sprint_result():
        # Choose a random number of completed points
        n_completed_sp = int(round(random.gauss(sp_mean, sp_std)))

        global completed_this_sprint
        global not_completed_this_sprint
        global n_iteration

        completed_this_sprint = []
        not_completed_this_sprint = []

        # Allocates completed points to stories according to their priorities
        # Moves stories in "sprint" accordingly
        aux = bl_sprint[:]
        for user_story in aux:

            # Points completed for this story
            story_completed_points = min(n_completed_sp, user_story.points - user_story.completed_points)

            # Update story
            n_completed_sp -= story_completed_points
            user_story.completed_points += story_completed_points

            # If the story has been completed, move it to "validation"
            if user_story.completed_points == user_story.points:
                bl_validation.append(user_story)
                completed_this_sprint.append(user_story)

            # If the user story was started it goes to "in progress"
            elif user_story.completed_points != 0:
                bl_in_progress.append(user_story)
                not_completed_this_sprint.append(user_story)

            # Otherwise it goes to "to do"
            else:
                bl_todo.append(user_story)
                not_completed_this_sprint.append(user_story)

            bl_sprint.remove(user_story)
            bl_sprint_selected = None


        global overall_completion_percentage
        overall_completion_percentage = int((len(bl_validation) + len(bl_done))*100 / (len(bl_validation) + len(bl_done) + len(bl_todo) + len(bl_in_progress) + len(bl_sprint)))

        update_project_statistics()

        n_iteration += 1

        check_if_reached_end_game()


    def update_project_statistics():
        pj_stats[0].value = overall_completion_percentage

        pj_stats[1].value = int((len(bl_validation) + len(bl_done))/n_iteration)

        print("Not all project statistics implemented yet by the way")

    def check_if_reached_end_game():

        # Bad ending condition: the iteration number is 8 or more. And the completion percentage is less then 60%
        if (n_iteration >= ct.NUM_SPRINTS_START_ENDING and overall_completion_percentage <= ct.MAX_COMPLETION_BAD_ENDING):
            renpy.call(label="bad_ending")

        # Good ending condition: 100% completion pergentage
    elif (n_iteration >= ct.NUM_SPRINTS_START_ENDING and overall_completion_percentage >= ct.MIN_COMPLETION_GOOD_ENDING):
            renpy.call(label="good_ending")

        # Else, end game condition is not reached, show "end of sprint" screen instead
        else:
            renpy.call(label="sprint_result")


## Backlog Navigation screen ###########################################################
##
## This screen is included in the backlog menus, and provides navigation
## to backlog menus

screen backlog_navigation():

    vbox:
        style_prefix "backlog_navigation"

        xpos gui.navigation_xpos
        yalign 0.5

        spacing gui.navigation_spacing


        textbutton _("A Fazer") action ShowMenu("a_fazer")

        textbutton _("Em Progresso") action ShowMenu("em_progresso")

        textbutton _("Corrida") action ShowMenu("corrida")

        textbutton _("Validação") action ShowMenu("validacao")

        textbutton _("Concluído") action ShowMenu("concluido")

        textbutton _("Estatísticas")  action ShowMenu("statistics")


style backlog_navigation_button is gui_button
style backlog_navigation_button_text is gui_button_text

style backlog_navigation_button:
    size_group "backlog_navigation"
    properties gui.button_properties("backlog_navigation_button")

style backlog_navigation_button_text:
    properties gui.button_text_properties("backlog_navigation_button")


## Backlog screen ###########################################################
##
## This screen is included in the backlog menus, and provides navigation
## to other menus.


screen backlog_menu(src, bl_list, selection, buttons=True):

    style_prefix "backlog_menu"

    add gui.game_menu_background

    frame:
        style "backlog_menu_outer_frame"

        hbox:

            ## Reserve space for the navigation section.
            frame:
                style "backlog_menu_navigation_frame"

            frame:
                style "backlog_menu_content_frame"

                viewport:
                    yinitial 0.0
                    scrollbars "vertical"
                    mousewheel True
                    draggable True
                    pagekeys True

                    side_yfill True

                    vbox:
                        transclude

        frame:
            style "backlog_menu_buttons_frame"

            if (src == "todo") or (src == "in_progress"):
                textbutton _("Selecionar"):
                    style "move_button"

                    action Function(add_to_sprint, bl_list, selection)

                    sensitive are_points_computed(selection, bl_list)

            elif src == "sprint":
                textbutton _("Remover"):
                    style "move_button"

                    action Function(remove_from_sprint, selection)

                    sensitive (bl_sprint_selected is not None)


            textbutton _("Orçar"):
                style "compute_button"

                action Function(compute_story_points, selection)

                if src != "todo":
                    sensitive False

            textbutton _("Priorizar"):
                style "prioritize_button"

                action Function(prioritize_user_story)

                sensitive (src == "sprint") and (bl_sprint_selected is not None) and (bl_sprint_selected != 0)


    use backlog_navigation

    textbutton _("Começar Corrida"):
        sensitive len(bl_sprint) > 0

        style "return_button"

        action Function(compute_sprint_result)

    label "Reserva de Requisitos"


screen statistics_menu():

    style_prefix "backlog_menu"

    add gui.game_menu_background

    frame:
        style "backlog_menu_outer_frame"

        hbox:

            ## Reserve space for the navigation section.
            frame:
                style "backlog_menu_navigation_frame"

            frame:
                style "backlog_menu_content_frame"

                viewport:
                    yinitial 0.0
                    scrollbars "vertical"
                    mousewheel True
                    draggable True
                    pagekeys True

                    side_yfill True

                    vbox:
                        transclude

    use backlog_navigation

    textbutton _("Começar Corrida"):
        style "return_button"

        action Function(compute_sprint_result)

    label "Estatísticas do Projeto"



style backlog_menu_outer_frame is empty
style backlog_menu_navigation_frame is empty
style backlog_menu_content_frame is empty
style backlog_menu_buttons_frame is empty
style backlog_menu_viewport is gui_viewport
style backlog_menu_side is gui_side
style backlog_menu_scrollbar is gui_vscrollbar

style backlog_menu_label is gui_label
style backlog_label_text is gui_label_text

style return_button is navigation_button
style return_button_text is navigation_button_text

style backlog_menu_outer_frame:
    bottom_padding 30
    top_padding 120

    background "gui/overlay/game_menu.png"

style backlog_menu_navigation_frame:
    xsize 280
    yfill True

style backlog_menu_content_frame:
    left_margin 40
    right_margin 20
    top_margin 10

style backlog_menu_buttons_frame:
    xpos 280
    xsize 440

style backlog_menu_viewport:
    xsize 920

style backlog_menu_vscrollbar:
    unscrollable gui.unscrollable

style backlog_menu_side:
    spacing 10

style backlog_menu_label:
    xpos 50
    ysize 120

style backlog_menu_label_text:
    size gui.title_text_size
    color gui.accent_color
    yalign 0.5

style return_button:
    xpos gui.navigation_xpos
    yalign 1.0
    yoffset -30

style prioritize_button:
    yalign 1.0
    xpos 330
    ypos 570

style compute_button:
    #xpos gui.navigation_xpos
    yalign 1.0
    xpos 30
    ypos 570

style move_button:
    yalign 1.0
    xpos 150
    ypos 570





screen a_fazer():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use backlog_menu("todo", bl_todo, bl_todo_selected):

        style_prefix "a_fazer"

        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_todo:
                textbutton user_story.get_text():
                    action ToggleSetMembership(bl_todo_selected, user_story)




## This is redefined in options.rpy to add text to the about screen.
define gui.a_fazer = ""


style a_fazer_label is gui_label
style a_fazer_label_text is gui_label_text
style a_fazer_text is gui_text
style a_fazer_button is check_button

style a_fazer_label_text:
    size gui.label_text_size



screen corrida():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use backlog_menu("sprint", bl_sprint, bl_sprint_selected):

        style_prefix "corrida"

        vbox:
            spacing gui.navigation_spacing

            for i, user_story in enumerate(bl_sprint):
                textbutton "{} - {}".format(i+1, user_story.get_text()):
                    action [SetVariable("bl_sprint_selected", i)]


## This is redefined in options.rpy to add text to the about screen.
define gui.corrida = ""


style corrida_label is gui_label
style corrida_label_text is gui_label_text
style corrida_text is gui_text
style corrida_button is check_button

style corrida_label_text:
    size gui.label_text_size



screen em_progresso():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use backlog_menu("in_progress", bl_in_progress, bl_in_progress_selected):

        style_prefix "em_progresso"

        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_in_progress:
                textbutton user_story.get_text():
                    action ToggleSetMembership(bl_in_progress_selected, user_story)


## This is redefined in options.rpy to add text to the about screen.
define gui.em_progresso = ""


style em_progresso_label is gui_label
style em_progresso_label_text is gui_label_text
style em_progresso_text is gui_text
style em_progresso_button is check_button

style em_progresso_label_text:
    size gui.label_text_size



screen validacao():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use backlog_menu("validation", bl_validation, bl_validation_selected):

        style_prefix "validacao"

        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_validation:
                textbutton user_story.get_text():
                    action ToggleSetMembership(bl_validation_selected, user_story)


## This is redefined in options.rpy to add text to the about screen.
define gui.validacao = ""


style validacao_label is gui_label
style validacao_label_text is gui_label_text
style validacao_text is gui_text
style validacao_button is check_button

style validacao_label_text:
    size gui.label_text_size



screen concluido():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use backlog_menu("done", bl_done, bl_done_selected, buttons=False):

        style_prefix "concluido"

        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_done:
                textbutton user_story.get_text():
                    sensitive False
                    #selected a == False


## This is redefined in options.rpy to add text to the about screen.
define gui.concluido = ""


style concluido_label is gui_label
style concluido_label_text is gui_label_text
style concluido_text is gui_text
style concluido_button is check_button

style concluido_label_text:
    size gui.label_text_size


screen statistics():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use statistics_menu():

        style_prefix "statistics"

        vbox:
            spacing gui.navigation_spacing

            for user_story in pj_stats:
                text user_story.get_text() + "\n"


## This is redefined in options.rpy to add text to the about screen.
define gui.statistics = ""


style statistics_label is gui_label
style statistics_label_text is gui_label_text
style statistics_text is gui_text
style statistics_button is check_button

style statistics_label_text:
    size gui.label_text_size
