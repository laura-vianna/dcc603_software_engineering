import constants
import random
import project_statistic_class as ps


### Game Entities
class UserStory:

    def __init__(self, category, base_points, description="Breve descricao", complete_bonus=None, delay_penalty={ps.CLIENT_SATISFACTION:0, ps.CODE_STABILITY:0, ps.TEAM_MOTIVATION:0}):
        self.category = category
        self.description = description
        self.base_points = base_points
        self.complete_bonus = complete_bonus
        self.delay_penalty = delay_penalty
        self.points = 0
        self.completed_points = 0

    def get_text(self):
        if(self.points == 0):
            return self.category.decode() + " - " + self.description

        return "(" + str(self.points) + ") " + self.category.decode() + " - " + self.description

    def compute_points(self, modifiers=None):
        if(self.points == 0):
            #self.points = random.choice(constants.USER_STORY_POINTS)
            self.points = self.base_points
