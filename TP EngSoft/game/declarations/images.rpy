####################################
    # Images declarations
####################################

label image_declarations:

    ###################
    # Character images
    ###################
    image businessman = "images/characters/businessman.png"

    ###################
    # Background images
    ###################
    image bad_ending = "images/backgrounds/bad_ending.jpeg"
    image changed_my_mind = "images/backgrounds/oh_no.jpg"
    image good_ending = "images/backgrounds/good_ending.jpg"
    image heavier_task = "images/backgrounds/oh_no.jpg"
    image initial_room = "images/backgrounds/initial_room.jpg"
    image lighter_task = "images/backgrounds/lighter_task.jpg"
    image new_bug = "images/backgrounds/new_bug.jpg"
    image project_scope_increased = "images/backgrounds/oh_no.jpg"
    image project_scope_reduced = "images/backgrounds/project_scope_reduced.jpg"
    image sick_dev = "images/backgrounds/sick_dev.jpg"
    image sprint_result_learn_more = "images/backgrounds/sprint_result_learn_more.jpeg"
    image sprint_result = "images/backgrounds/sprint_result.jpg"
