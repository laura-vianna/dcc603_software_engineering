import user_story_class as us

CLIENT_SATISFACTION = 0
CODE_STABILITY = 1
TEAM_MOTIVATION = 2

US_BONUS_POINTS = [-13, -8, -5, -3, -2, 0, 2, 3, 5, 8, 13]

class ProjectStatistic:
	def __init__(self, client_satisfaction, code_stability, team_motivation):
		self.client_satisfaction = client_satisfaction
		self.code_stability = code_stability
		self.team_motivation = team_motivation

	def get_text_list(self):
		return [b"Satisfa\xc3\xa7\xc3\xa3o do Usu\xc3\xa1rio: {}".format(self.client_satisfaction).decode(),
				b"Estabilidade do C\xc3\xb3digo: {}".format(self.code_stability).decode(),
				b"Motiva\xc3\xa7\xc3\xa3o da Equipe: {}".format(self.team_motivation).decode()]

	def update(self, us_left, us_done, us_points_base, us_points_requested):
		self.update_client_stisfaction(us_left, us_done)
		self.update_code_stability(us_left, us_done)
		self.update_team_motivation(us_points_base, us_points_requested)

	def update_client_stisfaction(self, us_left, us_done):
		# Compute the sum of client satisfaction bonuses from completed user stories and penalties from left user stories

		for user_story in us_left:
			if user_story.delay_penalty and CLIENT_SATISFACTION in user_story.delay_penalty:
				self.client_satisfaction = self.client_satisfaction - user_story.delay_penalty[CLIENT_SATISFACTION]

		for user_story in us_done:
			if user_story.complete_bonus and CLIENT_SATISFACTION in user_story.complete_bonus:
				self.client_satisfaction = self.client_satisfaction + user_story.complete_bonus[CLIENT_SATISFACTION]

		# Fit result to range
		if(self.client_satisfaction < 0):
			self.client_satisfaction = 0
		elif(self.client_satisfaction > 100):
			self.client_satisfaction = 100

	def update_code_stability(self, us_left, us_done):
		# Compute the sum of code stability bonuses from completed user stories and penalties from left user stories

		for user_story in us_left:
			if user_story.delay_penalty and CODE_STABILITY in user_story.delay_penalty:
				self.code_stability = self.code_stability - user_story.delay_penalty[CODE_STABILITY]

		for user_story in us_done:
			if user_story.complete_bonus and CODE_STABILITY in user_story.complete_bonus:
				self.code_stability = self.code_stability + user_story.complete_bonus[CODE_STABILITY]

		# Fit result to range
		if(self.code_stability < 0):
			self.code_stability = 0
		elif(self.code_stability > 100):
			self.code_stability = 100

	def update_team_motivation(self, us_points_base, us_points_requested):
		self.team_motivation = self.team_motivation + (us_points_base - us_points_requested) * 2

		# Fit result to range
		if(self.team_motivation < 0):
			self.team_motivation = 0
		elif(self.team_motivation > 100):
			self.team_motivation = 100

	def get_bonus_us_points(self):
		index = int(self.team_motivation / 10)

		return US_BONUS_POINTS[index]

	def get_modifier(self, stat_id):
		if(stat_id == CLIENT_SATISFACTION):
			return int((self.client_satisfaction - 50) * 0.1)
		elif(stat_id == CODE_STABILITY):
			return int((self.code_stability - 50) * 0.1)

		return 0