###############################################################################
# Sprint Result Python Functions
###############################################################################
def calculate_completion_percent(completed, not_completed):
    if not completed or len(completed) == 0:
        return 0

    if not not_completed or len(not_completed) == 0:
    	return 100;

    denominator = len(completed) + len(not_completed)
    if denominator <= 0:
        return 0

    return int(len(completed)*100 / denominator)
