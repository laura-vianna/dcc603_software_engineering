import json

nomes = {}
categorias = {}
descricoes = {}
opcionais = {}
pontosHistorias = {}
recompensas = {}
estabilidadeCodigos = {}
motivacaoEquipes = {}
satisfacaoClientes = {}

for i in range(1,14):
    nameFile = 'Historia' + str(i) + '.json'    
    with open(nameFile) as f:
        data = json.load(f)
    
    nomes[i] = data["nome"]
    categorias[i] = data["categoria"]
    descricoes[i] = data["descricao"]
    opcionais[i] = data["opcional"]
    pontosHistorias[i] = data["pontosHistoria"]
    recompensas[i] = data["recompensa"]
    estabilidadeCodigos[i] = data["recompensa"]["estabilidadeCodigo"]
    motivacaoEquipes[i] = data["recompensa"]["motivacaoEquipe"]
    satisfacaoClientes[i] = data["recompensa"]["satisfacaoCliente"]   
    

